def exercise_one():
    for k in range(1,101):

        if k%3 == 0 and k%5 == 0:
            print("ThreeFive")
        elif k%3 == 0 :
            print("Three")
        elif k%5 == 0:
            print("Five")
        else: print(k)

"""
Exercice 2
"""
def consecutive_subset_products(number):

    digits = [int(digit) for digit in str(number)]
    products = []
    for i in range(len(digits)):
        for j in range(i+1, len(digits)+1):
            subset = digits[i:j]
            subset_product = 1
            for digit in subset:
                subset_product *= digit
            products.append(subset_product) 
    return( products)

def doublons(liste):

    dico = {}
    for item in liste :

        if dico.get(item) == None :

            dico[item] = 1

        else :
            return False

    return True 

def exercise_two(number):

    print( doublons(consecutive_subset_products(number)))

"""
Exercice 3
"""

def exercise_three(strings_list):
    if type(strings_list) != type([]):
        print( False)
        return
    
    numbers = []
    for string in strings_list:
        # Essaye de convertir la string en nombre entier ou flottant
        if type(string) == str:
            try:
                number = int(string)
            except ValueError:

                number = None

            if number is not None:

                numbers.append(number)

    print( sum(numbers))


def deux_permutation(L1,L2): #gere aussi les doublons

    if len(L1)!= len(L2) :
        return(False)
    else :
        dico1 = {}
        for k in L1 :
            if dico1.get(k) == None :
                dico1[k] = 1
            else :
                dico1[k] += 1

        boleen = True
        for k in L2:
            if dico1.get(k) == None:
                return(False)
            else :
                if dico1[k] == 1 :
                    del dico1[k]
                else :
                    dico1[k] -= 1

        return(True)  # dit en O(n) si L1 permutation de L2 (liste ou str)

def anagrams(word, liste):

    result = []
    for mot in liste :

        if deux_permutation(mot,word):

            result.append(mot)
    return result

def excercise_four(word, liste):
    print(anagrams(word, liste))