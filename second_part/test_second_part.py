import pytest

from second_part.src import div, raise_something, add, ForceToList, random_gen, get_info#, CacheDecorator, func_exo4, func_exo4_2


def test_generator():
    g = random_gen()
    assert isinstance(g, type((x for x in [])))
    a = next(g)
    while a != 15:
        assert 10 <= a <= 20
        a = next(g)
    with pytest.raises(StopIteration):
        next(g)


def test_to_str():
    assert add(5, 30) == '35'
    assert get_info({'info': [1, 2, 3]}) == '[1, 2, 3]'


def test_ignore_exception():
    assert div(10, 2) == 5
    assert div(10, 0) is None
    assert raise_something(TypeError) is None
    with pytest.raises(NotImplementedError):
        raise_something(NotImplementedError)


def test_meta_list():
    test = ForceToList([1, 2])
    assert test[1] == 2
    assert test.x == 4

# def test_ex_4():
#     try:
#         func_exo4(10)
#     except:
#         print("The class is buggy, check the comment in src.py")
#     else: print('The class works !')
