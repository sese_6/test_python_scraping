
import random

def random_gen():
    number = 0
    while number != 15:
        number = random.randint(10, 20)
        yield number


def decorator_to_str(func):
    def wrapper(*args, **kwargs):
        return str(func(*args, **kwargs))
    return wrapper


@decorator_to_str
def add(a, b):
    return (a + b)


@decorator_to_str
def get_info(d):
    return (d['info'])


def ignore_exception(exception):
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                func(*args, **kwargs)

            except exception:
                return None
            
            else:
                return func(*args, **kwargs)
            
        return wrapper
    return decorator


@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b


@ignore_exception(TypeError)
def raise_something(exception):
    raise exception


#exercise 4
# class CacheDecorator:
#     """Saves the results of a function according to its parameters"""
#     def __init__(self): #def __init__(self, func):
#         self.cache = {}
#                         #self.func = func

#     def __call__(self, func):
#         def _wrap(*a, **kw):
#             if a[0] not in self.cache:
#                 self.cache[a[0]] = func(*a, **kw) #self.cache[a[0]] = self.func(*a, **kw)
#             return self.cache[a[0]]

#         return _wrap
    
# @CacheDecorator
# def func_exo4(a, b):
#     return a * b


class MetaInherList(type):
    def __new__(cls, name, bases, attrs):
        attrs['__bases__'] = (list,)
        return super().__new__(cls, name, bases, attrs)


class Ex:
    x = 4


class ForceToList(list, Ex, metaclass=MetaInherList):
    pass

class ProcessCheckerMeta(type):
    def __init__(cls, name, bases, attrs):
        if 'process' in attrs:
            process_method = attrs['process']
            if not callable(process_method):
                raise TypeError(f"'process' attribute of {name} class must be callable.")
            if not len(process_method.__code__.co_varnames) == 4:
                raise ValueError(f"'process' method of {name} class must take exactly 3 arguments.")
        super().__init__(name, bases, attrs)
