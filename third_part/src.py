import requests
import json
import csv
import logging
import os
import gzip
from decimal import Decimal

def http_request():
    headers = {'User-Agent': 'MyCustomUserAgent/1.0'}
    data = {'isadmin':'1'}
    response = requests.post('https://httpbin.org/anything', data, headers)
    return response.content

class ProductPrinter:
    def __init__(self):
        self.product_data = self.load_product_data()

    def load_product_data(self):
        data_path = os.path.join(os.path.dirname(__file__), '..', 'third_part', 'data', 'data.json.gz')
        with gzip.open(data_path, 'rb') as f:
            data = json.loads(f.read().decode('utf-8'))
        return data

    def print_available_products(self):
        logging.basicConfig(filename='product.log', level=logging.INFO)
        with open('available_products.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(['Product Name', 'Product Price'])

            for product in self.product_data["Bundles"]:
                try:
                    product['Product']
                except:
                    key = 'Products'
                else: key = 'Product'
                try:
                    product[key][0]["IsInStock"]
                except:
                    logging.error("Could not find information about product availability.")
                else:
                    if product[key][0]["IsInStock"] == True:
                        product_name = product[key][0]["Name"][:30]
                        product_price = round(Decimal(str(product[key][0]["Price"])), 1)
                        writer.writerow([product_name, product_price])
                    elif product[key][0]["IsInStock"] == False:
                        product_name = product[key][0]["Name"][:30]
                        product_code = product[key][0]["Barcode"]
                        logging.warning(f"Product {product_code} {product_name} is unavailable.")

